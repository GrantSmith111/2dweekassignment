using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    private Rigidbody2D rb;
    public Transform groundCheck;
    public Animator animator;
    public float speed;
    public float jumpForce;
    private bool canJump = true;
    private float count;

    public TextMeshProUGUI countText;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        count = 0;
    }




    void OnCollisionEnter2D(Collision2D coll)
    {
        canJump = true;

    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            Destroy(other.gameObject);
            count = count + 1;
        }
    }



    // Update is called once per frame
    void Update()
    {

        //animation code
        animator.SetFloat("Speed", rb.velocity.magnitude);
        //movement code
        Vector2 move = new Vector2();

        move.x = Input.GetAxis("Horizontal") * speed;

        if (Input.GetButtonDown("Jump"))
        {
            if (canJump == true)
            {
                rb.AddForce(new Vector2(0f, 50f) * jumpForce);
                canJump = false;
            }
        }
        rb.AddForce(move);
        countText.text = "Score: " + count.ToString();
    }
}
